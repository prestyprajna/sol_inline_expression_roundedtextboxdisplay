﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Sol_Inline_Expression_RoundedTextBoxDisplay.Entity
{
    public class PersonEntity
    {
        public string FirstName { get; set; }

        public string LastName { get; set; }
    }
}