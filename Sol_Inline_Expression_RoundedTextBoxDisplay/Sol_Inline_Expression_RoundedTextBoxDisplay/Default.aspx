﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="Sol_Inline_Expression_RoundedTextBoxDisplay.Default" %>

<%@ Import Namespace="Sol_Inline_Expression_RoundedTextBoxDisplay.ORD" %>
<%@ Import Namespace="Sol_Inline_Expression_RoundedTextBoxDisplay.Entity" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>

    <style>

        .labelSpan{
            width:70px;
            height:70px;
            background-color:mediumorchid;
            color:chartreuse;
            float:left;
            border-radius:200px;            
            text-align:center;
            margin:2px;
            padding:40px
        }
         .labelSpan1{
            width:70px;
            height:70px;
            background-color:mediumpurple;
            color:chartreuse;
            float:left;
            border-radius:200px;            
            text-align:center;
            margin:2px;
            padding:40px
        }


    </style>

</head>
<body>
    <form id="form1" runat="server">

    <div>

        <%
            var result = new PersonDAL().GetPersonData();
             %>

        <%
            foreach (var obj in result)
            {
             %>

        <div>

             <span class="labelSpan"> <%Response.Write(Server.HtmlEncode(obj.FirstName)); %></span>
            <span class="labelSpan1"><%Response.Write(Server.HtmlEncode(obj.LastName)); %></span>

        </div>
       

        <%
            }
             %>
    
    </div>

    </form>
</body>
</html>
