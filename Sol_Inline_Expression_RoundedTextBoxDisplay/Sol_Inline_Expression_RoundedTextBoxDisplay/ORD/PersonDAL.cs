﻿using Sol_Inline_Expression_RoundedTextBoxDisplay.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Sol_Inline_Expression_RoundedTextBoxDisplay.ORD
{
    public class PersonDAL
    {
        #region  declaration

        private PersonDCDataContext _dc = null;

        #endregion

        #region  constructor

        public PersonDAL()
        {
            _dc = new PersonDCDataContext();
        }

        #endregion

        #region  public methods

        public IEnumerable<PersonEntity> GetPersonData()
        {
            var getQuery =
                _dc
                ?.Persons
                ?.AsEnumerable()
                ?.Select((lePersonEntityObj) => new PersonEntity()
                {
                    FirstName = lePersonEntityObj?.FirstName,
                    LastName = lePersonEntityObj?.LastName
                })
                ?.Take(30)
                ?.ToList();

            return getQuery;
        }


        #endregion
    }
}